from flask import Flask, render_template

app = Flask(__name__)


@app.route('/hello/<user>')
def hello_name(user):
    return render_template('hello.html', name=user)


@app.route('/score/<int:score>')
def check_score(score):
    return render_template('score.html', marks=score)

@app.route('/creature')                                 #Why does endpoint not work?
def result():
   dict = {'XP':50,'Attack':15,'Block':25}
   return render_template('creature.html', result = dict)




@app.route('/')
def welcome():
    return "Welcome to The Game of Bones"


if __name__ == '__main__':
    app.run(debug=True)
